package nl.utwente.di.temperature;

public class Quoter {
    double getFahrenheit(String cel) {
        double temp = Double.parseDouble(cel);
        return (temp * 9/5) + 32;
    }
}
